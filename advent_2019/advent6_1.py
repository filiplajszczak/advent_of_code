from queue import Queue

tree = {}
with open("day6.in") as f:
    for line in f:
        pair = line.strip().split(")")
        try:
            tree[pair[0]].append(pair[1])
        except KeyError:
            tree[pair[0]] = [pair[1]]

r = {"COM": 0}
q = Queue()
q.put("COM")

while not q.empty():
    parent = q.get()
    for child in tree.get(parent, []):
        q.put(child)
        r[child] = r[parent] + 1

print(sum(r.values()))
