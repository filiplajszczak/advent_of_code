from advent_2019.advent5_2 import (
    add,
    parse_instruction,
    multiply,
    input_,
    output,
    jump_if_true,
    jump_if_false,
    less_than,
    equals,
)


def test_parse_instruction_parses_simple_add():
    assert parse_instruction(1) == (add, [0, 0], 4)


def test_parse_instruction_parses_parametrized_add():
    assert parse_instruction(1001) == (add, [0, 1], 4)


def test_parse_instruction_parses_simple_multiply():
    assert parse_instruction(2) == (multiply, [0, 0], 4)


def test_parse_instruction_parses_parametrized_multiply():
    assert parse_instruction(102) == (multiply, [1, 0], 4)


def test_parse_instruction_parses_simple_input():
    assert parse_instruction(3) == (input_, [0, 0], 2)


def test_parse_instruction_parses_simple_output():
    assert parse_instruction(4) == (output, [0, 0], 2)


def test_parse_instruction_parses_parametrized_output():
    assert parse_instruction(104) == (output, [1, 0], 2)


def test_parse_instruction_parses_simple_jump_if_true():
    assert parse_instruction(5) == (jump_if_true, [0, 0], 3)


def test_parse_instruction_parses_parametrized_jump_if_true():
    assert parse_instruction(1005) == (jump_if_true, [0, 1], 3)


def test_parse_instruction_parses_simple_jump_if_false():
    assert parse_instruction(6) == (jump_if_false, [0, 0], 3)


def test_parse_instruction_parses_parametrized_jump_if_false():
    assert parse_instruction(106) == (jump_if_false, [1, 0], 3)


def test_parse_instruction_parses_simple_less_than():
    assert parse_instruction(7) == (less_than, [0, 0], 4)


def test_parse_instruction_parses_parametrized_less_than():
    assert parse_instruction(107) == (less_than, [1, 0], 4)


def test_parse_instruction_parses_equals():
    assert parse_instruction(8) == (equals, [0, 0], 4)


def test_parse_instruction_parses_parametrized_equals():
    assert parse_instruction(108) == (equals, [1, 0], 4)
