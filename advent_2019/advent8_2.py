import textwrap

with open("day08.in") as f:
    data = f.read()

layers = textwrap.wrap(data, 150)
pixels = [list() for x in range(150)]


for layer in layers:
    for i, pixel in enumerate(layer):
        pixels[i].append(pixel)

result = []

for pixel in pixels:
    for number in pixel:
        if number != "2":
            result.append(number)
            break
    else:
        result.append("2")

for line in textwrap.wrap("".join(result), 25):
    print(line)
