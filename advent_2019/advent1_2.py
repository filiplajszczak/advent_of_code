def get_fuel(mass: int) -> int:
    fuel = mass // 3 - 2
    if fuel <= 0:
        return 0
    else:
        return fuel + get_fuel(fuel)


r = 0
with open("day1.in") as f:
    for line in f:
        r += get_fuel(int(line))
print(r)
