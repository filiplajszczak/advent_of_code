import itertools
from typing import List

from intcode import Intcode


def get_instructions(file: str) -> List:
    with open(file) as f:
        instructions = [int(x) for x in f.read().split(",")]
    return instructions


def execute(memory, stdin):
    intcode = Intcode(memory, stdin)
    return intcode.run()


def main() -> None:
    memory: List = get_instructions("day07.in")
    final_result = 0
    for sequence in itertools.permutations((0, 1, 2, 3, 4)):
        result = 0
        for stdin in sequence:
            result = execute(memory, [stdin, result])
        final_result = result if result > final_result else final_result
    print(final_result)


if __name__ == "__main__":
    main()
