from advent_2019.advent5_1 import add, parse_instruction, multiply, input_, output


def test_parse_instruction_parses_simple_add():
    assert parse_instruction(1) == (add, [0, 0, 0], 4)


def test_parse_instruction_parses_parametrized_add():
    assert parse_instruction(1001) == (add, [0, 1, 0], 4)


def test_parse_instruction_parses_simple_multiply():
    assert parse_instruction(2) == (multiply, [0, 0, 0], 4)


def test_parse_instruction_parses_parametrized_multiply():
    assert parse_instruction(102) == (multiply, [1, 0, 0], 4)


def test_parse_instruction_parses_simple_input():
    assert parse_instruction(3) == (input_, [0, 0, 0], 2)


def test_parse_instruction_parses_simple_output():
    assert parse_instruction(4) == (output, [0, 0, 0], 2)


def test_parse_instruction_parses_parametrized_output():
    assert parse_instruction(104) == (output, [1, 0, 0], 2)
