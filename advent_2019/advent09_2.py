from typing import List

from intcode import Intcode


def get_instructions(file: str) -> List:
    with open(file) as f:
        instructions = [int(x) for x in f.read().split(",")]
    return instructions


def execute(memory, stdin):
    intcode = Intcode(memory, stdin)
    result = None
    while not result:
        result = intcode.run()
    return result


def main() -> None:
    memory: List = get_instructions("day09.in")
    print(execute(memory, [2]))


if __name__ == "__main__":
    main()
