for noun in range(100):
    for verb in range(100):
        print(f"{noun}, {verb}")
        with open("day2.in") as f:
            program = {
                key: int(value)
                for key, value
                in enumerate(f.read().split(","))
            }
            program[1] = noun
            program[2] = verb
        address = 0
        while True:
            if program[address] == 99:
                break
            if program[address] == 1:
                result = program[program[address + 1]] + program[program[address + 2]]
                program[program[address + 3]] = result
                address += 4
                continue
            if program[address] == 2:
                result = program[program[address + 1]] * program[program[address + 2]]
                program[program[address + 3]] = result
                address += 4
                continue
        if program[0] == 19690720:
            print(noun * 100 + verb)
            quit()
