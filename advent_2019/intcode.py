import logging
from collections import defaultdict
from typing import List, Callable

logging.basicConfig(format="%(message)s")
logger = logging.getLogger(name=__name__)
logger.setLevel(logging.INFO)


class Intcode:
    def __init__(self, memory: List, stdin: List) -> None:
        self.memory = defaultdict(int)
        self.memory.update({key: value for key, value in enumerate(memory)})
        self.stdin = stdin
        self.stdout = []
        self.pointer = 0
        self.relative_base = 0
        self.state = "Initialized"

    def run(self):
        while True:
            logger.debug(self.relative_base)
            logger.debug([self.memory[self.pointer+x] for x in range(10)])
            if self.memory[self.pointer] == 99:
                self.state = "Halt"
                break
            elif self.stdout:
                self.state = "Running"
                break
            func = self.parse_instruction()
            func()
        try:
            return self.stdout.pop(0)
        except IndexError:
            return None

    def parse_instruction(self) -> Callable:
        instruction = self.memory[self.pointer]
        self.modes = [0, 0, 0]
        if len(str(instruction)) >= 3:
            opcode = int(str(instruction)[-2:])
            modes_dict = {
                i: int(x) for i, x in enumerate(reversed(str(instruction)[:-2]))
            }
            self.modes = [modes_dict.get(x, 0) for x in range(3)]
        else:
            opcode = instruction
        opcodes = {
            1: ["add", 4],
            2: ["multiply", 4],
            3: ["input_", 2],
            4: ["output", 2],
            5: ["jump_if_true", 3],
            6: ["jump_if_false", 3],
            7: ["less_than", 4],
            8: ["equals", 4],
            9: ["change_relative_base", 2],
        }
        func = getattr(self, opcodes[opcode][0])
        self.step = opcodes[opcode][1]
        return func

    def get_value(self, order):
        values = {
            0: self.memory[self.memory[self.pointer + order]],
            1: self.memory[self.pointer + order],
            2: self.memory[self.relative_base + self.memory[self.pointer + order]],
        }
        return values[self.modes[order - 1]]

    def get_target(self, order):
        targets = {
            0: self.memory[self.pointer + order],
            2: self.relative_base + self.memory[self.pointer + order],
        }
        return targets[self.modes[order - 1]]

    def add(self) -> None:
        first = self.get_value(1)
        second = self.get_value(2)
        target = self.get_target(3)
        self.memory[target] = first + second
        self.pointer += self.step

    def multiply(self) -> None:
        first = self.get_value(1)
        second = self.get_value(2)
        target = self.get_target(3)
        self.memory[target] = first * second
        self.pointer += self.step

    def input_(self) -> None:
        value = self.stdin.pop(0)
        target = self.get_target(1)
        self.memory[target] = value
        self.pointer += self.step

    def output(self) -> None:
        value = self.get_value(1)
        self.stdout.append(value)
        self.pointer += self.step

    def jump_if_true(self) -> None:
        first = self.get_value(1)
        second = self.get_value(2)
        if first == 0:
            self.pointer += self.step
        else:
            self.pointer = second

    def jump_if_false(self) -> None:
        first = self.get_value(1)
        second = self.get_value(2)
        if first != 0:
            self.pointer += self.step
        else:
            self.pointer = second

    def less_than(self) -> None:
        first = self.get_value(1)
        second = self.get_value(2)
        target = self.get_target(3)
        value = 1 if first < second else 0
        self.memory[target] = value
        self.pointer += self.step

    def equals(self) -> None:
        first = self.get_value(1)
        second = self.get_value(2)
        target = self.get_target(3)
        value = 1 if first == second else 0
        self.memory[target] = value
        self.pointer += self.step

    def change_relative_base(self) -> None:
        first = self.get_value(1)
        self.relative_base += first
        self.pointer += self.step
