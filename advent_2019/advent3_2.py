def draw(grid, wire_on_grid, pos, length):
    if not wire_on_grid.get(pos):
        wire_on_grid[pos] = [1, length]
    else:
        wire_on_grid[pos][0] = 2
    if wire_on_grid[pos][0] == 1:
        if not grid.get(pos):
            grid[pos] = 1
        else:
            grid[pos] = 2

    return grid, wire_on_grid


with open("day3.in") as f:
    grid = {}
    wires_on_grid = [dict(), dict()]
    for i, line in enumerate(f):
        pos = (0, 0)
        wire = line.split(",")
        length = 0
        for move in wire:
            for step in range(int(move[1:])):
                length += 1
                if move[0] == "U":
                    pos = pos[0] + 1, pos[1]
                elif move[0] == "D":
                    pos = pos[0] - 1, pos[1]
                elif move[0] == "L":
                    pos = pos[0], pos[1] - 1
                elif move[0] == "R":
                    pos = pos[0], pos[1] + 1
                grid, wires_on_grid[i] = draw(grid, wires_on_grid[i], pos, length)


crosses = [pos for pos in grid.keys() if grid[pos] == 2]
print(min([wires_on_grid[0][pos][1] + wires_on_grid[1][pos][1] for pos in crosses]))
