with open("day2.in") as f:
    program = {
        key: int(value)
        for key, value
        in enumerate(f.read().split(","))
    }
    program[1] = 12
    program[2] = 2
address = 0
while True:
    if program[address] == 99:
        break
    if program[address] == 1:
        result = program[program[address + 1]] + program[program[address + 2]]
        program[program[address + 3]] = result
        address += 4
        continue
    if program[address] == 2:
        result = program[program[address + 1]] * program[program[address + 2]]
        program[program[address + 3]] = result
        address += 4
        continue
print(program[0])
