import textwrap

with open("day08.in") as f:
    data = f.read()

layers = [(layer, layer.count("0")) for layer in textwrap.wrap(data, 150)]

result_layer = min(layers, key=lambda x: x[1])[0]

print(result_layer.count("1") * result_layer.count("2"))

