import re


def ascending(x):
    return x == int("".join(sorted(list(str(x)))))


def pair(x):
    return bool(re.search(r"(.)\1", str(x)))


def check(x):
    return ascending(x) and pair(x)


with open("day4.in") as f:
    range_ = [int(x) for x in f.read().split("-")]

result = len([check(x) for x in range(range_[0], range_[1] + 1) if check(x)])

print(result)

