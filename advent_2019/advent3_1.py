def draw(grid, wire_on_grid, pos):
    if not wire_on_grid.get(pos):
        wire_on_grid[pos] = 1
    else:
        wire_on_grid[pos] = 2
    if wire_on_grid[pos] == 1:
        if not grid.get(pos):
            grid[pos] = 1
        else:
            grid[pos] = 2
    return grid, wire_on_grid


with open("day3.in") as f:
    grid = {}
    for line in f:
        wire_on_grid = {}
        pos = (0, 0)
        wire = line.split(",")
        for move in wire:
            for step in range(int(move[1:])):
                if move[0] == "U":
                    pos = pos[0] + 1, pos[1]
                elif move[0] == "D":
                    pos = pos[0] - 1, pos[1]
                elif move[0] == "L":
                    pos = pos[0], pos[1] - 1
                elif move[0] == "R":
                    pos = pos[0], pos[1] + 1
                grid, wire_on_grid = draw(grid, wire_on_grid, pos)

crosses = [abs(pos[0]) + abs(pos[1]) for pos in grid.keys() if grid[pos] == 2]
print(min(crosses))