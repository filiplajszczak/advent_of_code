import sys
from typing import List, Callable


def get_instructions() -> List:
    with open("day5.in") as f:
        instructions = [int(x) for x in f.read().split(",")]
    return instructions


def parse_instruction(instruction) -> (Callable, dict, int):
    modes = [0, 0,]
    if len(str(instruction)) >= 3:
        opcode = int(str(instruction)[-2:])
        modes_dict = {i: int(x) for i, x in enumerate(reversed(str(instruction)[:-2]))}
        modes = [modes_dict.get(x, 0) for x in range(2)]
    else:
        opcode = instruction
    opcodes = {
        1: ["add", 4],
        2: ["multiply", 4],
        3: ["input_", 2],
        4: ["output", 2],
        5: ["jump_if_true", 3],
        6: ["jump_if_false", 3],
        7: ["less_than", 4],
        8: ["equals", 4],
    }
    func = getattr(sys.modules[__name__], opcodes[opcode][0])
    return func, modes, opcodes[opcode][1]


def add(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    target = memory[pointer + 3]
    return {"target": target, "value": first + second}


def multiply(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    target = memory[pointer + 3]
    return {"target": target, "value": first * second}


def input_(memory: List, pointer: int, modes: list) -> dict:
    value = 5
    target = memory[pointer + 1]
    return {"target": target, "value": value}


def output(memory: List, pointer: int, modes: list) -> dict:
    return {"output": memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]}


def jump_if_true(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    if first == 0:
        return {"jump": "do_nothing"}
    else:
        return {"jump": second}


def jump_if_false(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    if first != 0:
        return {"jump": "do_nothing"}
    else:
        return {"jump": second}


def less_than(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    target = memory[pointer + 3]
    value = 1 if first < second else 0
    return {"target": target, "value": value}


def equals(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    target = memory[pointer + 3]
    value = 1 if first == second else 0
    return {"target": target, "value": value}


def program_loop(memory: List) -> int:
    pointer = 0
    while True:
        print(pointer)
        print(memory[pointer: pointer+9])
        print(memory[224])
        if memory[pointer] == 99:
            break
        func, modes, step = parse_instruction(memory[pointer])
        change = func(memory, pointer, modes)

        if jump := change.get("jump"):
            if jump == "do_nothing":
                pointer += step
            else:
                pointer = jump
            continue
        try:
            result = change["output"]
        except KeyError:
            memory[change["target"]] = change["value"]
        pointer += step
    return result


def main() -> None:
    memory: List = get_instructions()
    result: int = program_loop(memory)
    print(result)


if __name__ == "__main__":
    main()
