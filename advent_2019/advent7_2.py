import itertools
from typing import List

from intcode import Intcode


def get_instructions(file: str) -> List:
    with open(file) as f:
        instructions = [int(x) for x in f.read().split(",")]
    return instructions


def get_intcodes(memory, sequence):
    intcodes = []
    result = 0
    for i, stdin in enumerate(sequence):
        intcodes.append(Intcode(memory, [stdin, result]))
        result = intcodes[i].run()
    return intcodes, result


def main() -> None:
    memory: List = get_instructions("day07.in")
    final_result = 0
    for sequence in itertools.permutations((5, 6, 7, 8, 9)):
        intcodes, result = get_intcodes(memory, sequence)
        while True:
            for i, intcode in enumerate(intcodes):
                intcode.stdin.append(result)
                output = intcode.run()
                result = output if output is not None else result
                if i == 4 and intcode.state == "Halt":
                    break
            if intcodes[4].state == "Halt":
                break
        final_result = result if result > final_result else final_result
    print(final_result)


if __name__ == "__main__":
    main()
