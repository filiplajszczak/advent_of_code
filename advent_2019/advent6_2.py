def get_path_to_com_set(tree, start_node):
    next_ = tree[start_node]
    path_to_com = set()
    while True:
        if next_ == "COM":
            break
        path_to_com.add(tree[next_])
        next_ = tree[next_]
    return path_to_com


tree = {}
with open("day6.in") as f:
    for line in f:
        pair = line.strip().split(")")
        tree[pair[1]] = pair[0]

you_to_com = get_path_to_com_set(tree, "YOU")
san_to_com = get_path_to_com_set(tree, "SAN")

common_path = you_to_com & san_to_com
you_unique_path = len(you_to_com) - len(common_path)
san_unique_path = len(san_to_com) - len(common_path)
result = 2 + san_unique_path + you_unique_path
print(result)
