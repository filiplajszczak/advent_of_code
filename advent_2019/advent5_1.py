import sys
from typing import List, Callable


def get_instructions() -> List:
    with open("day5.in") as f:
        instructions = [int(x) for x in f.read().split(",")]
    return instructions


def parse_instruction(instruction) -> (Callable, int, int, int, int):
    modes = [0, 0, 0]
    if len(str(instruction)) >= 3:
        opcode = int(str(instruction)[-2:])
        modes_dict = {i: int(x) for i, x in enumerate(reversed(str(instruction)[:-2]))}
        modes = [modes_dict.get(x, 0) for x in range(3)]
    else:
        opcode = instruction
    if opcode == 1:
        func_name = "add"
        step = 4
    elif opcode == 2:
        func_name = "multiply"
        step = 4
    elif opcode == 3:
        func_name = "input_"
        step = 2
    elif opcode == 4:
        func_name = "output"
        step = 2
    func = getattr(sys.modules[__name__], func_name)
    return func, modes, step


def add(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    target = memory[pointer + 3]
    return {
        "target": target,
        "value": first + second
    }


def multiply(memory: List, pointer: int, modes: list) -> dict:
    first = memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]
    second = memory[pointer + 2] if modes[1] else memory[memory[pointer + 2]]
    target = memory[pointer + 3]
    return {
        "target": target,
        "value": first * second
    }


def input_(memory: List, pointer: int, modes: list) -> dict:
    value = 1
    target = memory[pointer + 1]
    return {
        "target": target,
        "value": value
    }


def output(memory: List, pointer: int, modes: list) -> dict:
    return {"output": memory[pointer + 1] if modes[0] else memory[memory[pointer + 1]]}


def program_loop(memory: List) -> int:
    pointer = 0
    result = []
    while True:
        if memory[pointer] == 99:
            break
        func, modes, step = parse_instruction(memory[pointer])
        change = func(
            memory, pointer, modes
        )

        try:
            result.append(change["output"])
        except KeyError:
            memory[change["target"]] = change["value"]
        pointer += step
    return result[-1]


def main() -> None:
    memory: List = get_instructions()
    result: int = program_loop(memory)
    print(result)


if __name__ == "__main__":
    main()
