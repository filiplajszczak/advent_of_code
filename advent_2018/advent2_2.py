with open("day2.in") as f:
    ids = []
    for line in f:
        ids.append(line)
    for line in ids:
        for pair in ids:
            if line == pair:
                continue
            diffs = 0
            result = ""
            for line_char, pair_char in zip(line, pair):
                if line_char != pair_char:
                    diffs += 1
                else:
                    result += line_char
                if diffs >= 2:
                    break
            if diffs == 1:
                print(result)
                quit()
