twos = 0
threes = 0

with open("day2.in") as f:
    for line in f:
        chars = {}
        for char in line.strip():
            if char in chars.keys():
                chars[char] += 1
            else:
                chars[char] = 1
        for key, value in chars.items():
            if value == 3:
                threes += 1
                break
        for key, value in chars.items():
            if value == 2:
                twos += 1
                break

print(twos * threes)
