points = []
locations = []
infinite = set()
first = True
with open("day6.in") as f:
    for line in f:
        x, y = map(int, line.split(", "))
        if first:
            maxx = x
            minx = x
            maxy = y
            miny = y
            first = False
        if x < minx:
            minx = x
        if x > maxx:
            maxx = x
        if y < miny:
            maxy = y
        if y > maxy:
            maxy = y
        points.append((x, y))
        locations.append(0)


def distance(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


for x in range(minx-(maxx-minx), maxx+(maxx-minx)+1):
    for y in range(miny-(maxy-miny), maxy+(maxy-miny)+1):
        distances = []
        for i, point in enumerate(points):
            distances.append(distance(point[0], point[1], x, y))
        mindis = min(distances)
        if len([i for i, e in enumerate(distances) if e == mindis]) == 1:
            locations[distances.index(mindis)] += 1
            if (
                x == minx-(maxx-minx)
                or x == maxx+(maxx-minx)
                or y == miny-(maxy-miny)
                or y == maxy+(maxy-miny)
            ):
                infinite.add(distances.index(mindis))

for i in infinite:
    locations[i] = 0

print(max(locations))
