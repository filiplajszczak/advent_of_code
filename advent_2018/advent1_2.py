l = []
s = set()
r = 0

with open("day1.in", "r") as f:
    for line in f:
        l.append(int(line))
while True:
    for n in l:
        r += n
        if r in s:
            print(r)
            quit()
        s.add(r)
