a = 0
for i in range(16):
    l = [int(n) for n in input().split()]
    for n in range(len(l)):
        for m in range(len(l)):
            if l[n] != l[m]:
                if l[n] % l[m] == 0:
                    a += l[n]//l[m]

print(a)
