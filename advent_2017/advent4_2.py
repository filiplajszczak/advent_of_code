a = 0
for i in range(512):
    l = input().split()
    b = 0
    for word in l:
        lminus = l[:]
        lminus.remove(word)
        wl = sorted(list(word))
        for n in lminus:
            nl = sorted(list(n))
            if wl == nl:
                b += 1

    if b == 0:
        a += 1

print(a)