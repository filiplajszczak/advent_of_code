a = 0
for i in range(16):
    l = [int(_) for _ in input().split()]
    a += (max(l) - min(l))

print (a)
